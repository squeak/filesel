const fs = require("node:fs");
const fsPromises = require("node:fs/promises");
const notifier = require("node-notifier");
const _ = require("underscore");
const selected = require("./selected");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = async function (pathModificationFunction) {

  var nonRenamedFiles = "";
  var renamedFiles = [];
  let filesPaths = await selected.files();
  if (!filesPaths || !filesPaths.length) throw "failed to find the path(s) of file(s) to rename"

  // renamed each selected file (or if not possible add them to list of files not renamed)
  await _.reduce(filesPaths, async function (memo, filePath) {
    await memo;
    let resultPath = await pathModificationFunction(filePath);
    if (fs.existsSync(resultPath)) nonRenamedFiles += `\n>  ${filePath.match(/[^/]*$/)}`
    else {
      await fsPromises.rename(filePath, resultPath);
      renamedFiles.push(`>  ${resultPath.match(/[^/]*$/)}`);
    };
  }, undefined);

  // notify renamed or errors
  if (nonRenamedFiles) throw `Some files were not renamed, because the destination exist already!${nonRenamedFiles}`
  else notifier.notify({
    title: "Successfully renamed files:",
    message: renamedFiles.join("\n"),
    urgency: "normal",
  });

};
