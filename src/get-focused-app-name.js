const util = require("node:util");
const exec = util.promisify(require("node:child_process").exec);
const _ = require("underscore");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = async function () {

  let command = "ps -e | grep $(xdotool getwindowpid $(xdotool getwindowfocus)) | grep -v grep | awk '{print $4}'";
  // var command = "ps -e | grep $(xdotool getwindowpid $(xdotool getactivewindow)) | grep -v grep | awk '{print $4}'";


  let { stdout, stderr } = await exec(command);
  if (stderr) throw `failed get currently focused app name: ${stderr}`
  else return _.isString(stdout) ? stdout.replace(/\n*$/, "") : stdout;

}
