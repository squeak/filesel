# filesel

A nodejs library to manipulate currently selected files and folders.


## Installation

```
git clone https://framagit.org/squeak/filesel.git
cd filesel
npm install
chmod u+x ./filesel
```


## Usage

Cd in filesel directory and run:
`./filesel --help`
to see all available options.


## Additional utilities

### get-focused-app-name

#### Description
Get the name of the app currently focused.

#### Usage
```javascript
const getFocusedAppName = require("filesel/src/get-focused-app-name");
getFocusedAppName(function(focusedAppName){
  console.log(focusedAppName); // nautilus
});
```


### rename-selected

#### Description
Rename the currently selected file.

#### Usage
```javascript
const renameSelectedFiles = require("filesel/src/rename-selected");
renameSelectedFiles(function(oldPath, callback){
  callback(oldPath.replace(/\.js$/, ".css")); // /path/to/file.js > /path/to/file.css
});
```


### selected-files

#### Description
A simple node module to get the current selection (for native apps and scripts, it doesn't work in the browser).  
This module actually returns the current selection in any program, but it will test if the selection is a proper array of paths. The module is mainly intended to get the paths of the currently selected files in nautilus (or any file browser you're using).

#### Install
**⚠ This module depend on xdotool.**  
Xdotool is probably available on any unix system.  
For example, to install it on debian/ubuntu system it should be as simple as running: `sudo apt install xdotool`.

#### Usage
```javascript
const selectedFiles = require("filesel/src/selected-files");
selectedFiles(function(filesPaths){
  console.log(filesPaths) // [ "/path/to/file/selected", "/path/to/file/selectedAsWell" ]
});
```
